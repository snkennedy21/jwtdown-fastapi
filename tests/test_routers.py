from jose import jwt
from requests.utils import dict_from_cookiejar
from .dict_app import dict_client
from .model_app import model_client
import pytest


@pytest.mark.parametrize("client", [dict_client, model_client])
def test_dict_login_works_for_good_password(client):
    response = client.post(
        "/token", data={"username": "noor", "password": "password"}
    )
    assert response.status_code == 200


@pytest.mark.parametrize("client", [dict_client, model_client])
def test_dict_login_fails_for_bad_password(client):
    response = client.post(
        "/token", data={"username": "noor", "password": "not a good one"}
    )
    assert response.status_code == 401
    assert response.json() == {"detail": "Incorrect username or password"}


@pytest.mark.parametrize("client", [dict_client, model_client])
def test_good_login_to_dict_client_has_cookie_with_account_data_and_no_password(client):
    response = client.post(
        "/token", data={"username": "noor", "password": "password"}
    )
    cookies = dict_from_cookiejar(response.cookies)
    data = jwt.decode(
        cookies["fastapi_token"], "secret key", algorithms=["HS256"]
    )

    assert response.status_code == 200
    assert data["sub"] == data["account"]["email"]
    assert data["account"]["email"] == "noor"
    assert data["account"]["age"] == 30
    assert "hashed_password" not in data["account"]
    assert "hashed_password_foo" not in data["account"]
    assert "foo_password_hashed" not in data["account"]
    assert "password" not in data["account"]


@pytest.mark.parametrize("client", [dict_client, model_client])
def test_login_must_have_both_username_and_password(client):
    response = client.post(
        "/token", data={"password": "password"}
    )
    assert response.status_code == 422

    response = client.post(
        "/token", data={"username": "noor"}
    )
    assert response.status_code == 422


@pytest.mark.parametrize("client", [dict_client, model_client])
def test_good_login_to_dict_client_has_json_response(client):
    response = client.post(
        "/token", data={"username": "noor", "password": "password"}
    )
    data = response.json()
    assert response.status_code == 200
    assert data["access_token"] == response.cookies.get("fastapi_token")
    assert data["token_type"] == "Bearer"


@pytest.mark.parametrize("client", [dict_client, model_client])
def test_logout_after_login_removes_cookie(client):
    login_response = client.post(
        "/token", data={"username": "noor", "password": "password"}
    )
    cookies = {"fastapi_token": login_response.cookies.get("fastapi_token")}
    logout_response = client.delete(
        "/token",
        data={"username": "noor", "password": "password"},
        cookies=cookies,
    )
    assert logout_response.status_code == 200
    assert logout_response.json()
    assert logout_response.cookies.get("fastapi_token") is None


@pytest.mark.parametrize("client", [dict_client, model_client])
def test_logout_without_login_still_returns_true(client):
    response = client.delete(
        "/token",
        data={"username": "noor", "password": "password"},
    )
    assert response.status_code == 200
    assert response.json()
    assert response.cookies.get("fastapi_token") is None


@pytest.mark.parametrize("client", [dict_client, model_client])
def test_access_protected_resource_without_logging_in_is_401(client):
    response = client.get("/protected")
    assert response.status_code == 401
    assert response.json() == {"detail": "Invalid token"}


@pytest.mark.parametrize("client", [dict_client, model_client])
def test_access_protected_after_login_with_cookie_is_200(client):
    login = client.post(
        "/token", data={"username": "noor", "password": "password"}
    )
    cookies = {"fastapi_token": login.cookies.get("fastapi_token")}
    response = client.get("/protected", cookies=cookies)
    assert response.status_code == 200
    assert response.json() == {"email": "noor", "age": 30}


@pytest.mark.parametrize("client", [dict_client, model_client])
def test_access_protected_after_login_with_bearer_token_is_200(client):
    login = client.post(
        "/token", data={"username": "noor", "password": "password"}
    )
    headers = {"Authorization": f"Bearer {login.cookies.get('fastapi_token')}"}
    response = client.get("/protected", headers=headers)
    assert response.status_code == 200
    assert response.json() == {"email": "noor", "age": 30}


@pytest.mark.parametrize("client", [dict_client, model_client])
def test_access_public_resource_without_logging_in_is_200(client):
    response = client.get("/not_protected")
    assert response.status_code == 200
    assert response.json() is None


@pytest.mark.parametrize("client", [dict_client, model_client])
def test_access_public_after_login_with_cookie_is_200(client):
    login = client.post(
        "/token", data={"username": "noor", "password": "password"}
    )
    cookies = {"fastapi_token": login.cookies.get("fastapi_token")}
    response = client.get("/protected", cookies=cookies)
    assert response.status_code == 200
    assert response.json() == {"email": "noor", "age": 30}


@pytest.mark.parametrize("client", [dict_client, model_client])
def test_access_public_after_login_with_bearer_token_is_200(client):
    login = client.post(
        "/token", data={"username": "noor", "password": "password"}
    )
    headers = {"Authorization": f"Bearer {login.cookies.get('fastapi_token')}"}
    response = client.get("/protected", headers=headers)
    assert response.status_code == 200
    assert response.json() == {"email": "noor", "age": 30}
